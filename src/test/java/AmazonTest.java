import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AmazonTest {

   private WebDriver driver;

   @Before
   public void abrir() {
      System.setProperty("webdriver.gecko.driver","C:/Windows/geckodriver.exe");
      driver = new FirefoxDriver();
      driver.manage().window().maximize();
      driver.get("https://www.amazon.com.br/");
   }

   @After
   public void sair() {
      driver.quit();
   }

   @Test
   public void maisVendidos() throws InterruptedException {
      Thread.sleep(3000);
      driver.findElement(By.id("nav-hamburger-menu")).click();
      Thread.sleep(3000);
      driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
      Thread.sleep(3000);
      Assert.assertEquals("Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
   }

   @Test
   public void testarBuscador() throws InterruptedException {
      driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
      driver.findElement(By.id("nav-search-submit-button")).click();
      Thread.sleep(3000);
      Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
      Thread.sleep(3000);
   }

   @Test
   public void TestarLoginComErro() {
      driver.findElement(By.id("nav-link-accountList")).click();
      driver.findElement(By.id("ap_email")).sendKeys("fabricafs@gmail.com");
      driver.findElement(By.id("continue")).click();
      Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail",driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
   }

   @Test
   public void BuscarLivro() throws InterruptedException {
      driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[5]")).click();  //clicar em livros
      Thread.sleep(3000);
      driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[2]/div[2]/div/div[2]/ul/li[8]/span/a/span")).click(); //clicar em livros de computação
      Thread.sleep(3000);
      driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[3]/span/a/span")).click();   //clicar em livros de analise de sistemas
      Assert.assertEquals("Livros de Análise de Sistemas e Design",driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div/h1/b")).getText());
      Thread.sleep(3000);
   }

   @Test
   public void NovidadesAmazon() throws InterruptedException {
      driver.findElement(By.id("nav-hamburger-menu")).click();  //clicar em todos
      Thread.sleep(3000);
      driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click(); //clicar em novidades na amazon
      Thread.sleep(3000);
      Assert.assertEquals("Novidades na Amazon",driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
      Thread.sleep(3000);
   }

   @Test
   public void ProdutosEmAlta() throws InterruptedException {
      driver.findElement(By.id("nav-hamburger-menu")).click();   //CLICAR EM TODOS
      Thread.sleep(3000);
      driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[4]/a")).click();  //CLICAR EM PRODUTOS EM ALTA
      Thread.sleep(3000);
      Assert.assertEquals("Produtos em alta",driver.findElement(By.id("zg_banner_text")).getText());
   }

   @Test
   public void PreencherEVerificarCEP() throws InterruptedException {
      driver.findElement(By.id("nav-global-location-popover-link")).click(); //CLICAR EM SELECIONAR O ENDEREÇO
      Thread.sleep(3000);
      driver.findElement(By.id("GLUXZipUpdateInput_0")).sendKeys("58340000");  //PREENCHER O CEP
      Thread.sleep(3000);
      driver.findElement(By.id("GLUXZipUpdate")).click();  //CLICAR EM CONFIRMAR
      Thread.sleep(3000);
      Assert.assertEquals("58340000\u200C",driver.findElement(By.id("glow-ingress-line2")).getText());   //VALIDAR O RESULTADO
      Thread.sleep(3000);
   }
}